<?php
/*
Template Name: Front Page
*/

get_header();
?>
    <main class="frontpage">

        <div class="row margin-0">
            <?php if (have_posts()) : ?>
                <div class="col-md-6 col-12 sticky-top frontpage-container_left">
                    <?php

                    // The Query
                    query_posts(array(
                        'post_type' => array('journeys'),
                        'posts_per_page' => 1,
                    ));
                    while (have_posts()) :
                        the_post(); ?>
                        <div class="bg"
                             style="background-image: url('<?php the_post_thumbnail_url(); ?>'); background-size: cover;">
                            <div class="bottom">
                                <h1 class="title"><?php the_title(); ?></h1>
                                <p class="intro"><?php the_excerpt(); ?></p>
                                <div>
                                    <a class="btn btn-white" href="<?php the_permalink(); ?>">My Journey</a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile;

                    // Reset Query
                    wp_reset_query(); ?>

                </div>
            <?php endif; ?>
            <div class="col-md-6 padding-0 col-12">
                <?php
                while (have_posts()) :
                    the_post();

                    the_content();


                endwhile;
                ?>
            </div>

        </div>
    </main>


<?php
get_footer();
