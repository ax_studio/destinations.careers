<?php

function create_posttype()
{

    register_post_type('journeys',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Journeys'),
                'singular_name' => __('Journey')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'journeys'),
            'menu_icon' => 'dashicons-location',
            'menu_position' => 2,
            'supports' => array('title', 'editor', 'author', 'excerpt', 'thumbnail', 'revisions', 'custom-fields','page-attributes'),

        )
    );

}

// Hooking up our function to theme setup
add_action('init', 'create_posttype');