function textareaAutoGrow() {


    $('textarea').attr('rows', '1');

    $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
}

function onScroll() {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 70) {
            $('.header').addClass('change');
        } else {
            $('.header').removeClass('change');

        }

    });
}

function menuToggle() {
    $('.menu-wrapper').click(function() {
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $('.header-navigation').addClass('change');
        } else {
            $('.header-navigation').removeClass('change');
        }
    });
}